import {
  ApiErrorCode,
  IReportRequest,
  PeekdataApi,
  ReportSortDirectionType,
} from "peekdata-datagateway-api-sdk";
import React from "react";
import ReactDOM from "react-dom";
import { ITranslations, ReportBuilder } from "react-report-builder";

// Include main report builder styles
import "react-report-builder/lib/main.css";

// Bootstrap (https://getbootstrap.com/docs/3.4/css/) styles are mandatory for
// report builder layout
import "bootstrap/dist/css/bootstrap.min.css";

// react-table (https://github.com/tannerlinsley/react-table/tree/v6) is used
// to show tabular report results. See react-table docs for style customization
import "react-table/react-table.css";

// react-datepicker (https://github.com/Hacker0x01/react-datepicker/) is used
// for date fields. See react-datepicker docs for style customization
import "react-datepicker/dist/react-datepicker.css";

// #region -------------- Constants -------------------------------------------------------------------

// Report builder component uses PeekdataApi instance from peekdata-datagateway-api-sdk package.
// Configure api parameters here
const peekdataApi = new PeekdataApi({
  // Url to Peekdata API service
  // baseUrl: "https://demo.peekdata.io:8443/datagateway/rest/v1",
  baseUrl: "http://localhost:8080/datagateway/rest/v1",
});

// Request parameters for report generation.
// See https://peekdata.io/developers/index.html#datagateway-api-sdk-request-options for more details

// const reportRequest: Partial<IReportRequest> = {
//   scopeName: "Mortgage-Lending",
//   dataModelName: "Servicing-PostgreSQL",
//   dimensions: ["cityname", "currency", "countryname"],
//   metrics: ["loanamount", "propertyprice"],
//   filters: {
//     dateRanges: [
//       {
//         from: "2015-01-01",
//         to: "2017-12-31",
//         key: "closingdate",
//       },
//     ],
//   },
//   sortings: {
//     dimensions: [
//       {
//         key: "cityname",
//         direction: ReportSortDirectionType.ASC,
//       },
//     ],
//   },
// };

// You can provide your own translations for every label in report builder.
const newTranslations: Partial<ITranslations> = {
  scopesDropdownTitle: "ビジネススコープ/ドメインを選択してください",
  scopesPlaceholder: "範囲",
  dataModelsDropdownTitle: "情報元",
  dataModelsPlaceholder: "情報元",
  contentTitle: "レポートのオプション",
  dimensionsListTitle: "外形寸法",
  dimensionPlaceholder: "寸法を選択",
  noDimensionsText: "寸法が見つかりません",
  addDimensionButtonText: "ディメンションを追加",
  metricsListTitle: "指標",
  metricPlaceholder: "指標を選択",
  noMetricsText: "指標が見つかりません",
  addMetricButtonText: "指標を追加",
  filtersText: "フィルター",
  addFilterButton: "フィルターを追加",
  optionalLabel: "オプション",
  rowsOffset: "行から始める",
  rowsLimit: "行数を制限",
  chartTab: "チャート",
  tableTab: "テーブル",
  filterTypePlaceholder: "フィルタータイプ",
  filterOperationPlaceholder: "操作",
  filterFromLabel: "から",
  filterToLabel: "に",
  filterValuesDescription: "可能な値はセミコロンで区切る必要があります。",
  filterValuesExample: "例",
  filterTypeDateRange: "日付範囲",
  filterTypeSingleKey: "単一キー",
  filterTypeSingleValue: "単一の値",
  filterSingleKeyPlaceholder: "キー",
  filterSingleValueKeysPlaceholder: "キー",
  filterSingleValuePlaceholder: "値",
  viewRequestPayload: "リクエストのペイロードを表示",
  viewRequestAsCURL: "リクエストをcURLとして表示",
  viewResponseAsOptimizedDataJson: "応答を最適化されたJSONとして表示",
  viewResponseAsFullDataJson: "完全なJSONとして応答を表示する",
  viewResponseAsSQL: "応答をSQLとして表示",
  viewResponseAsCSV: "応答をCSVとして表示",
  copyToClipboardButton: "クリップボードにコピー",
  copiedToClipboardMessage: "クリップボードにコピーしました",
  chartTypeBar: "バー",
  chartTypeLine: "ライン",
  chartTypePie: "パイ",
  chartTypeDoughnut: "ドーナツ",
  chartTypeRadar: "レーダー",
  tablePreviousText: "前",
  tableNextText: "次",
  tableLoadingText: "読み込み中...",
  tableNoDataText: "行が見つかりません",
  tablePageText: "ページ",
  tableOfText: "の",
  tableRowsText: "行",
  tablePageJumpText: "ページにジャンプ",
  tableRowsSelectorText: "ページあたりの行数",
  apiErrors: {
    [ApiErrorCode.BadDataModelName]: "不正なデータモデル名",
  },
};

// #endregion

// #region -------------- Component -------------------------------------------------------------------

interface IProps {}

interface IState {
  translations: Partial<ITranslations>;
}

class App extends React.PureComponent<IProps, IState> {
  public render() {
    return (
      <div>
        {/* Button to demonstrate translation changing */}
        <input
          type="button"
          value="Change translations"
          onClick={this.onChangeTranslations}
        />

        {/* Provide prop "reportRequest={reportRequest}" to render this component with preset template*/}
        <ReportBuilder
          peekdataApi={peekdataApi}
          defaultRowsOffset={0}
          defaultRowsLimit={100}
          maxRowsLimit={200}
          showRequestViewButton={true}
          showResponseViewButton={true}
          showDataTabs={true}
        />
      </div>
    );
  }

  private onChangeTranslations = () => {
    this.setState({ translations: newTranslations });
  };
}

// #endregion

ReactDOM.render(
  <div style={{ padding: "20px" }}>
    <App />
  </div>,
  document.getElementById("root")
);
